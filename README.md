﻿# Booking.Bot

模擬情境為下訂書籍的chatbot，使用bot framework v4開發

## 環境

- [.NET Core SDK](https://dotnet.microsoft.com/download) version 2.1

## 測試/執行

- Bot Framework Emulator version 4.5.0 or greater [here](https://github.com/Microsoft/BotFramework-Emulator/releases)

