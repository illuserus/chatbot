﻿using Microsoft.EntityFrameworkCore;
namespace Booking.Models
{
    public class StoreContext : DbContext
    {
        public StoreContext(DbContextOptions<StoreContext> options) : base(options)
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlite("Data Source=StoreContext.db");
        }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Member> Members { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Order>()
                .Property(x => x.isCancel)
                .HasDefaultValue(false);
#if DEBUG
            /*
             * 種子測試資料
             */

            /*
             * 書籍
             */
            modelBuilder.Entity<Book>().HasData(
                new Book { Id = 1, ISBN = "0011654654", Author = "Elizabeth" , Name = "Scala程式設計", Stock = 10 },
                new Book { Id = 2, ISBN = "0102213211", Author = "ColdBlood" , Name = "Spark實戰機器學習", Stock = 0 },
                new Book { Id = 3, ISBN = "0102213312", Author = "Samuel" , Name = "專家洞察力Python機器學習", Stock = 20 }
                );
            /*
             * 使用者
             */
            modelBuilder.Entity<Member>().HasData(
                new Member { Id = "A123456789", Name = "王大明", Mobile = "123456", EMail = "test@gmail.com" }
                );
#endif
        }

    }
}
