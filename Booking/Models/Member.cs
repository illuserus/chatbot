﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Booking.Models
{
    public class Member
    {
        /// <summary>
        /// 身分證字號
        /// </summary>
        [MaxLength(10),Required]
        public string Id { get; set; }
        /// <summary>
        /// 姓名
        /// </summary>
        [MaxLength(20)]
        public string Name { get; set; }
        /// <summary>
        /// EMail
        /// </summary>
        [EmailAddress,Required]
        public string EMail { get; set; }
        /// <summary>
        /// 電話號碼
        /// </summary>
        [Phone,Required]
        public string Mobile { get; set; }
        
        
    }
}
