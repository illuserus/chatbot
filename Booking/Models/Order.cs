﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Booking.Models
{
    public class Order
    {
        [Key]
        public long Id { get; set; }
        public string MemeberId { get; set; }
        public bool isCancel { get; set; }
        public ICollection<Book> Books { get; set; }
    }
}
