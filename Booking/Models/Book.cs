﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Booking.Models
{
    public class Book
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public string ISBN { get; set; }
        public int Stock { get; set; }

        
    }
}
