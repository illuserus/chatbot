﻿using Booking.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Booking.Services
{
    public interface IBookingService
    {
        Member CurrentMember { get; }
        /// <summary>
        /// 查詢書本
        /// </summary>
        /// <param name="lambda"></param>
        /// <returns></returns>
        ICollection<Book> QueryBooks(Expression<Func<Book,bool>> lambda);
        /// <summary>
        /// 查詢訂單
        /// </summary>
        /// <param name="lambda"></param>
        /// <returns></returns>
        ICollection<Order> QueryOrder(Expression<Func<Order, bool>> lambda);
        /// <summary>
        /// 下訂
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        bool Booking(Order order);
        /// <summary>
        /// 取消訂單
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        bool CancelBooking(Order order);
        /// <summary>
        /// 新增會員
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        bool SignUp(Member member);
        /// <summary>
        /// 登入
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        bool SignIn(string Id,string Email);
        

    }
}
