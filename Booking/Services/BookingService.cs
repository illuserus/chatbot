﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Booking.Models;

namespace Booking.Services
{
    public class BookingService : IBookingService
    {
        protected StoreContext _context;
        protected Member _member;
        public Member CurrentMember => this._member;
        public BookingService(StoreContext context)
        {
            this._context = context;
            this._member = null;
        }

        public bool Booking(Order order)
        {
            bool res = true;
            try
            {
                this._context.Orders.Add(order);
            }
            catch
            {
                res = false;
            }
            return res;
        }

        public bool CancelBooking(Order order)
        {
            order.isCancel = true;
            return true;
        }

        public ICollection<Book> QueryBooks(Expression<Func<Book, bool>> lambda)
        {
            return this._context.Books.Where(lambda).ToList();
        }

        public ICollection<Order> QueryOrder(Expression<Func<Order, bool>> lambda)
        {
            string memberId = this._member.Id;
            return this._context.Orders.Where(x => x.MemeberId == memberId).Where(lambda).ToList();
        }

        public bool SignIn(string Id,string Email)
        {
            bool res = true;
            try
            {
                this._member = this._context.Members.Where(x => x.Id == Id && x.EMail == Email).First();
                res = true;
            }
            catch
            {
                this._member = null;
                res = false;
            }
            return res;
        }

        public bool SignUp(Member member)
        {
            this._context.Members.Add(member);
            return this._context.SaveChanges() > 1;
        }
    }
}
