// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
//
// Generated with Bot Builder V4 SDK Template for Visual Studio EchoBot v4.5.0

using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Booking.Bot.Dialogs;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;

namespace Booking.Bot.Bots
{
    public class EchoBot : ActivityHandler
    {
        protected Dialog _mainDialog;
        protected BotState _conversationState;
        protected BotState _userState;
        public EchoBot(ConversationState conversationState, UserState userState,MainDialog dialog)
        {
            this._mainDialog = dialog;
            this._conversationState = conversationState;
            this._userState = userState;
        }
        protected override async Task OnMessageActivityAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            await this._mainDialog.RunAsync(turnContext, this._conversationState.CreateProperty<DialogState>(nameof(DialogState)), cancellationToken);
        }
        public override async Task OnTurnAsync(ITurnContext turnContext, CancellationToken cancellationToken = default(CancellationToken))
        {
            await base.OnTurnAsync(turnContext, cancellationToken);
            //儲存目前聊天狀態
            await this._conversationState.SaveChangesAsync(turnContext, false, cancellationToken);
            await this._userState.SaveChangesAsync(turnContext, false, cancellationToken);
        }

        protected override async Task OnMembersAddedAsync(IList<ChannelAccount> membersAdded, ITurnContext<IConversationUpdateActivity> turnContext, CancellationToken cancellationToken)
        {
            var reply = MessageFactory.Text("您好，我是訂書小精靈。很高興為您服務");
            foreach (var member in membersAdded)
            {
                if (member.Id != turnContext.Activity.Recipient.Id)
                {
                    await turnContext.SendActivityAsync(reply, cancellationToken);
                }
            }
        }
    }
}
