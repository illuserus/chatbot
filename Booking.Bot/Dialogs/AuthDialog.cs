﻿using Booking.Models;
using Booking.Services;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Booking.Bot.Dialogs
{
    public class AuthDialog : ComponentDialog
    {
        protected UserState _userState;
        protected IBookingService _service;
        public AuthDialog(UserState userState,IBookingService service) :base (nameof(AuthDialog))
        {
            this._userState = userState;
            this._service = service;

            this.AddDialog(new ConfirmPrompt(nameof(ConfirmPrompt)));
            this.AddDialog(new TextPrompt(nameof(TextPrompt)));

            this.AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                AskId,
                AskEmail,
                AskName,
                AskMobile,
                Confirm,
                Finish

            }));
            InitialDialogId = nameof(WaterfallDialog);
        }

        protected async Task<DialogTurnResult> AskId(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            stepContext.Values[nameof(Member)] = new Member();
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions
            {
                Prompt = MessageFactory.Text("請輸入您的身分證字號"),
            }, cancellationToken);
        }


        protected async Task<DialogTurnResult> AskEmail(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var member = (Member)stepContext.Values[nameof(Member)];
            member.Id = stepContext.Result.ToString();


            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions
            {
                Prompt = MessageFactory.Text("請輸入您的Email"),
            }, cancellationToken);
        }

        protected async Task<DialogTurnResult> AskName(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var member = (Member)stepContext.Values[nameof(Member)];
            member.EMail = stepContext.Result.ToString();
            if (this._service.SignIn(member.Id, member.EMail))
            {
                member = this._service.CurrentMember;
                stepContext.Values[nameof(Member)] = member;
                await stepContext.Context.SendActivityAsync(MessageFactory.Text("您先前已於本服務註冊，因此無須再核對"));
                await stepContext.Context.SendActivityAsync(MessageFactory.Text("歡迎您 " + member.Name));
                return await stepContext.EndDialogAsync(member, cancellationToken);
            }
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions
            {
                Prompt = MessageFactory.Text("請輸入您的姓名"),
            }, cancellationToken);
        }

        protected async Task<DialogTurnResult> AskMobile(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var member = (Member)stepContext.Values[nameof(Member)];
            member.Name = stepContext.Result.ToString();
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions
            {
                Prompt = MessageFactory.Text("請輸入您的手機號碼"),
            }, cancellationToken);
        }

        protected async Task<DialogTurnResult> Confirm(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var member = (Member)stepContext.Values[nameof(Member)];
            member.Mobile = stepContext.Result.ToString();

            string MemberInfo = "身份證字號 : " + member.Id + 
                " , 姓名 : " + member.Name + 
                " , Email : " + member.EMail +
                " , 手機號碼 : " + member.Mobile;

            await stepContext.Context.SendActivityAsync(MessageFactory.Text("請您核對資料，您輸入的資料如下 : "));
            await stepContext.Context.SendActivityAsync(MessageFactory.Text(MemberInfo));
            return await stepContext.PromptAsync(nameof(ConfirmPrompt), new PromptOptions
            {
                Prompt = MessageFactory.Text("請問資料是否正確?"),
            }, cancellationToken);
        }

        protected async Task<DialogTurnResult> Finish(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            if((bool)stepContext.Result)
            {
                var member = (Member)stepContext.Values[nameof(Member)];
                await stepContext.Context.SendActivityAsync(MessageFactory.Text("感謝您的配合"));
                return await stepContext.EndDialogAsync(member,cancellationToken);
            }
            else
            {
                await stepContext.Context.SendActivityAsync(MessageFactory.Text("再一次核對您的資料"));
                return await stepContext.BeginDialogAsync(nameof(WaterfallDialog),cancellationToken);
            }
        }
    }
}
