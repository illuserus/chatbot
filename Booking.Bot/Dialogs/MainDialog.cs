﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Booking.Models;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Choices;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Logging;

namespace Booking.Bot.Dialogs
{
    public class MainDialog : ComponentDialog
    {
        class FunctionType
        {
            public const string Booking = "訂書";
            public const string QueryBook = "查詢書籍";
            public const string QueryOrder = "查詢訂單";
            public const string CancelOrder = "取消訂單";
        }
        protected UserState _userState;
        public MainDialog(UserState userState,QueryBookDialog queryBookDialog,OrderDialog orderDialog) : base(nameof(MainDialog))
        {
            this._userState = userState;

            this.AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            this.AddDialog(queryBookDialog);
            this.AddDialog(orderDialog);
            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                ShowMenuAsync,
                SelectFunctionAsync,

            }));
            InitialDialogId = nameof(WaterfallDialog);
        }

        protected async Task<DialogTurnResult> ShowMenuAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            return await stepContext.PromptAsync(nameof(ChoicePrompt),
                new PromptOptions
                {
                    Prompt = MessageFactory.Text("請問您需要甚麼服務?"),
                    RetryPrompt = MessageFactory.Text("我沒有此服務，請重新選擇"),
                    Choices = ChoiceFactory.ToChoices(
                        new List<string>()
                        {
                            FunctionType.Booking,
                            FunctionType.QueryBook, 
                            FunctionType.QueryOrder,
                            FunctionType.CancelOrder
                        }),
                }, cancellationToken);
        }

        protected async Task<DialogTurnResult> SelectFunctionAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            string dialogName = "";
            FoundChoice choice = (FoundChoice)stepContext.Result;
            switch (choice.Value)
            {
                case FunctionType.Booking:
                    dialogName = nameof(OrderDialog);
                    break;
                case FunctionType.QueryOrder:
                    dialogName = "";
                    break;
                case FunctionType.QueryBook:
                    dialogName = nameof(QueryBookDialog);
                    break;
                case FunctionType.CancelOrder:
                    dialogName = "";
                    break;
                default:
                    await stepContext.Context.SendActivityAsync(MessageFactory.Text("很抱歉，仍然無法辨識您的操作"),cancellationToken);
                    return await stepContext.EndDialogAsync();
            }
            return await stepContext.BeginDialogAsync(dialogName, cancellationToken);

            //var prompt = new PromptOptions() {
            //    Prompt = MessageFactory.Text(stepContext.Result.ToString())
            //};
            //return await stepContext.PromptAsync(nameof(TextPrompt), prompt, cancellationToken);

        }
    }
}
