﻿using Booking.Models;
using Booking.Services;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Choices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Booking.Bot.Dialogs
{
    public class OrderDialog : ComponentDialog
    {
        private const string ExitMessage = "已經完成了，結帳";
        protected UserState _userState;
        protected IBookingService _service;


        public OrderDialog(UserState userState,StoreContext context, IBookingService service,AuthDialog authDialog,QueryBookDialog queryBookDialog) : base(nameof(OrderDialog))
        {
            
            this._userState = userState;
            this._service = service;

            this.AddDialog(authDialog);
            this.AddDialog(queryBookDialog);

            this.AddDialog(new TextPrompt(nameof(TextPrompt)));
            this.AddDialog(new ConfirmPrompt(nameof(ConfirmPrompt)));

            this.AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                ReqAuth,
                AddBook,
                ShowBookDetail,
                BookConfirm,
                Continue,
                MakeOrder
            })); ;
            InitialDialogId = nameof(WaterfallDialog);

        }

        public async Task<DialogTurnResult> ReqAuth(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var isAuth = stepContext.Values.Keys.Contains(nameof(Member));
            if (!isAuth)
            {
                await stepContext.Context.SendActivityAsync(MessageFactory.Text("歡迎您使用訂書服務"), cancellationToken);
                await stepContext.Context.SendActivityAsync(MessageFactory.Text("在您下單前，需先查核您的身分"), cancellationToken);
                return await stepContext.BeginDialogAsync(nameof(AuthDialog), null, cancellationToken);
            }
            else
            {
                return await stepContext.NextAsync(isAuth, cancellationToken);
            }
        }

        public async Task<DialogTurnResult> AddBook(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            
            var member = (Member)stepContext.Result;
            stepContext.Values[nameof(Member)] = member;
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions() {
                 Prompt = MessageFactory.Text("請輸入您欲訂購的書籍ISBN碼"),
                 Choices = ChoiceFactory.ToChoices(new List<string>() { ExitMessage })
            }, cancellationToken);
        }

        public async Task<DialogTurnResult> ShowBookDetail(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            string keyword = stepContext.Result.ToString();
            string resp = "";
            try
            {
                Book book= this._service.QueryBooks(x => x.ISBN.Contains(keyword) && x.Stock>0).First();
                stepContext.Values[nameof(Book)] = book;
                resp = "您輸入的書籍為 : " + string.Join(",", book.Name, book.Author);
            }
            catch
            {
                resp = "很抱歉，您的書籍目前不在庫或已銷售一空";
                stepContext.Values[nameof(Book)] = null;
                return await stepContext.NextAsync(false, cancellationToken);
            }
            await stepContext.Context.SendActivityAsync(MessageFactory.Text(resp));
            return await stepContext.PromptAsync(nameof(ConfirmPrompt), new PromptOptions()
            {
                Prompt = MessageFactory.Text("加入訂單?"),
            }, cancellationToken);

        }

        public async Task<DialogTurnResult> BookConfirm(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            if ((bool)stepContext.Result)
            {
                bool isOrderCreate = stepContext.Values.ContainsKey(nameof(Order));
                Order order = null;
                if(!isOrderCreate)
                {
                    order = new Order();
                    order.Books = new List<Book>();
                }
                else
                {
                    order = (Order)stepContext.Values[nameof(Order)];
                }
                var book = (Book)stepContext.Values[nameof(Book)];
                order.Books.Add(book);
                stepContext.Values[nameof(Order)] = order;
            }
            return await stepContext.PromptAsync(nameof(ConfirmPrompt), new PromptOptions()
            {
                Prompt = MessageFactory.Text("繼續訂書嗎?")
            }, cancellationToken);
        }

        public async Task<DialogTurnResult> Continue(WaterfallStepContext stepContext ,CancellationToken cancellationToken)
        {

            if ((bool)stepContext.Result)
            {
                var order = (Order)stepContext.Values[nameof(Order)];
                stepContext.ActiveDialog.State["stepIndex"] = (int)stepContext.ActiveDialog.State["stepIndex"] - 2;
                return await stepContext.ReplaceDialogAsync(nameof(WaterfallDialog),order, cancellationToken);
            }
            else
            {
                return await stepContext.NextAsync(null, cancellationToken);
            }

        }
        public async Task<DialogTurnResult> MakeOrder(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var order = (Order)stepContext.Values[nameof(Order)];
            bool res = this._service.Booking(order);
            
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions()
            {
                Prompt = MessageFactory.Text("已為您建立訂單"),
            }, cancellationToken);
        }



    }
}
