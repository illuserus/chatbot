﻿using Booking.Models;
using Booking.Services;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Choices;
using Microsoft.Bot.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Booking.Bot.Dialogs
{
    public class QueryBookDialog : ComponentDialog
    {
        class QueryType
        {
            public const string ByName = "依書名搜尋";
            public const string ByAuthor = "依作者搜尋";
            public const string ByISBN = "依ISBN搜尋";
            public const string Exit = "不用了，我有書籍資訊了";
        }
        class QueryState
        {
            public string QueryType { get; set; }
            public string Keyword { get; set; }
        }

        protected UserState _userState;
        protected IBookingService _service;
        public QueryBookDialog(UserState userState,IBookingService service,AuthDialog authDialog) :base (nameof(QueryBookDialog))
        {
            this._userState = userState;
            this._service = service;

            this.AddDialog(authDialog);

            this.AddDialog(new ChoicePrompt(nameof(ChoicePrompt)));
            this.AddDialog(new TextPrompt(nameof(TextPrompt)));

            this.AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                SelectQueryMode,
                QueryBooks,
                ShowResult,
            }));
            InitialDialogId = nameof(WaterfallDialog);
        }

        public async Task<DialogTurnResult> SelectQueryMode(WaterfallStepContext stepContext,CancellationToken cancellationToken)
        {
            return await stepContext.PromptAsync(nameof(ChoicePrompt), new PromptOptions
            {
                Prompt = MessageFactory.Text("請選擇搜尋方式?"),
                RetryPrompt = MessageFactory.Text("我沒有此服務，請重新選擇"),
                Choices = ChoiceFactory.ToChoices(new List<string>() {
                    QueryType.ByAuthor,
                    QueryType.ByName,
                    QueryType.ByISBN,
                    QueryType.Exit
                })
            }, cancellationToken);
        }
        public async Task<DialogTurnResult> QueryBooks(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            FoundChoice choice = (FoundChoice)stepContext.Result;
            if(choice.Value == QueryType.Exit)
            {
                await stepContext.Context.SendActivityAsync(MessageFactory.Text("很高興為您服務"));
                return await stepContext.EndDialogAsync();
            }
            var query = await this._userState.CreateProperty<QueryState>(nameof(QueryState)).GetAsync(stepContext.Context, () => new QueryState());
            query.QueryType = choice.Value;
            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions
            {
                Prompt = MessageFactory.Text("請輸入您的關鍵字"),
                Choices = ChoiceFactory.ToChoices(new List<string>() { QueryType.Exit})
            }, cancellationToken);
        }

        public async Task<DialogTurnResult> ShowResult(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var query = await this._userState.CreateProperty<QueryState>(nameof(QueryState)).GetAsync(stepContext.Context, () => new QueryState());
            query.Keyword = stepContext.Result.ToString();


            List<Book> Books = null;
            switch (query.QueryType)
            {
                case QueryType.ByAuthor:
                    Books = this._service.QueryBooks(x => x.Author.Contains(query.Keyword)).ToList();
                    break;
                case QueryType.ByISBN:
                    Books = this._service.QueryBooks(x => x.ISBN.Contains(query.Keyword)).ToList();
                    break;
                case QueryType.ByName:
                    Books = this._service.QueryBooks(x => x.Name.Contains(query.Keyword)).ToList();
                    break;
            }

            string res = "";
            if (Books.Count > 0)
            {
                Books.ForEach(x => {
                    res += string.Join(",", x.ISBN, x.Author, x.Name, x.Stock);
                });
            }
            else
            {
                res = "很抱歉，您的關鍵字查無資料";
            }

            return await stepContext.PromptAsync(nameof(TextPrompt), new PromptOptions {
                Prompt = MessageFactory.Text(res)
            }, cancellationToken);
        }
    }
}
